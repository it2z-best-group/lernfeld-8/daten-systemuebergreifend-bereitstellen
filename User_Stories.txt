Vorlage:
"Als [Kundentyp] [möchte] ich, [damit]."

1) Als Manager möchte ich sehen können, wie viele Menschen an dem Quiz teilnehmen, damit ich nachvollziehen kann, wie es im
Verhältnis mit neu gewonnenen Kunden steht.

2) Als Mitarbeiter möchte ich sehen können, wie oft Fragen richtig oder falsch beantwortet werden, damit ich gegebenenfalls
den Schwierigkeitsgrad anpassen kann.

3) Als Mitarbeiter möchte ich die Möglichkeit haben, durch ein simples Interface selbst Quizfragen zu
bearbeiten / hinzufügen / entfernen, ohne dafür die Datenbank öffnen zu müssen.

4) Als Kunde möchte ich die Möglichkeit haben, auf eine sehr simple Art und Weise an dem Quiz teilzunehmen, damit mich der
Aufwand nicht vom Spielen abhält.

5) Als Kunde möchte ich, dass nur so wenig personenbezogene Daten wie nötig gespeichert werden, damit ich kein schlechtes Gefühl
habe an dem Quiz teilzunehmen.

6) Als Kunde möchte ich die möglichkeit haben, meine Freunde / Bekannte einzuladen, um direkt gegen sie zu spielen.

7) Als Manager möchte ich, dass das Logo der Firma in der App oft sichtbar ist, damit die Assoziation der App zur Firma gegeben
ist.

8) Als Manager möchte ich, dass einige Fragen der App auf die Firma / Produkte der Firma beziehen, damit die Assoziation der
App zur Firmma gegeben ist.
